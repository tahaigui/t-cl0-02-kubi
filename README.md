##### connect to the aks ######
az aks get-credentials --resource-group rg-kubi --name aks-kubi

##### activation de l'agic ######
Pour activation de AGIC voir : https://learn.microsoft.com/fr-fr/azure/application-gateway/tutorial-ingress-controller-add-on-existing?toc=https%3A%2F%2Flearn.microsoft.com%2Ffr-fr%2Fazure%2Faks%2Ftoc.json&bc=https%3A%2F%2Flearn.microsoft.com%2Ffr-fr%2Fazure%2Fbread%2Ftoc.json#enable-the-agic-add-on-in-existing-aks-cluster-through-azure-portal

az aks enable-addons -n aks-kubi -g rg-kubi  --subscription "394e6431-b5e0-4004-b173-bc15e0379ebf" -a ingress-appgw --appgw-id "/subscriptions/394e6431-b5e0-4004-b173-bc15e0379ebf/resourceGroups/rg-kubi/providers/Microsoft.Network/applicationGateways/agic-appgw"

##### install angular ######
helm install angular -n applications --create-namespace ./values/angularjs

##### install laravel ######
helm install laravel -n applications --create-namespace ./values/laravel

##### install elasticsearch ######
helm install elasticsearch -n applications --create-namespace ./values/elasticsearch

##### install indexer ######
helm install indexer -n applications --create-namespace ./values/indexer

##### install reporting ######
helm install reporting -n applications --create-namespace ./values/reporting

##### après chaque apply de l'infra ne pas oublier de faire le changement de dns !!!!!